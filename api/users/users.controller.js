const express = require('express');
const router = express.Router();
const userService = require('./user.service');
const authoritiesService = require("./authorities.service");

const isAllowed = require("../auth/permission");

/*  has role Admin*/
router.post('/users', isAllowed("Admin"), createUser);
router.get('/users', isAllowed("Admin"), getAll);
router.put('/users', updateUser);
router.get('/users/:login', getByLoginName);
router.delete('/users/:login', deleteByLoginName);

/*  for Auth */
router.get('/users/authorities', getAllAuthorities);
router.post('/users/authorities', createAuthority);

router.get('/current', getCurrent);
/*router.get('/:id', getById);*/
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function createUser(req, res, next) {
    /* temporary use same service   */
    console.log(req.body);
    userService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}
function createAuthority(req, res, next) {
    /* temporary use same service   */
    console.log(req.body);
    authoritiesService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll(req, res)
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getAllAuthorities(req, res, next) {
    authoritiesService.getAll()
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getByLoginName(req, res, next) {
        console.log(req.user);
    userService.getByloginName(req.params.login)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}


function updateUser(req, res, next) {
    console.log(req.body);
    userService.update(req.body.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function deleteByLoginName(req, res, next) {
    userService.deleteByLoginName(req.params.login)
        .then(() => res.json({}))
        .catch(err => next(err));
}
