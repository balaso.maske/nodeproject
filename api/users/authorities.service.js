const config = {
    "connectionString": "mongodb://localhost/nodeproject",
    "secret": "5QdYLbXsONJJDkoEyQPk3aR7vng721veBww9pPglW1tRzp4nnUz3f4AewUPZl9YyyK590vdErx2KBfTu66GOrUOZs6CO9XzMAmQ8hddNqA7oVuV0cUuvEl7Hskqbofgr"
};

const db = require('../_helpers/db');
const Authorities = db.Authorities;

module.exports = {
    getAll,
    create
};


async function getAll() {
    const data = await Authorities.find();
    return data;
}
    

async function create(param) {

    const authority = Authorities(param);
    // save authority
    await authority.save();
}
