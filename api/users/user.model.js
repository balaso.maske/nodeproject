const mongoose = require('mongoose');
var validate = require('mongoose-validator');
const Schema = mongoose.Schema;

var nameValidator = [
    validate({
      validator: 'isLength',
      arguments: [1, 50],
      message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters',
    }),
    validate({
      validator: 'isAlphanumeric',
      passIfEmpty: true,
      message: 'Name should contain alpha-numeric characters only',
    }),
];

const schema = new Schema({
    login: {
        type: String,
        unique: true,
        required: [true, 'Login is required']
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'E-Mail is required']
    },
    password: {
      type: String,
      required: [true, 'Password is required']
    },
    activated: {
        type: Boolean,
        required: true
    },
    firstName: {
        type: String,
        default: null
    }, 
    lastName: {
        type: String,
        default: null
    },
    langKey: {
        type: String
    },
    imageUrl: {
        type: String,
        default: ""
    },
    activationKey: {
        type: String,
        default: ""
    },
    resetKey: {
        type: String,
        default: ""
    },
    resetDate: {
        type: Date,
        default: Date.now
    },
    createdBy: {
        type: String,
        default: ""
    },
    lastModifiedBy: {
        type: String,
        default: ""
    },
    lastModifiedDate: {
        type: Date,
        default: Date.now
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    authorities: [
        { 
            type: Schema.Types.ObjectId,
            ref: 'user_authority'
        }
    ]
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);