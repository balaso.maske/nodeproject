const express = require('express');
const router = express.Router();
const accountService = require('./account.service');
const Exception = require("../errors/Exception");
const constant = require("../default/constants").Constant;


const isAllowed = require("../auth/permission");

router.post('/register', register);
router.get('/activate', activateUser);
router.post('/authenticate', authenticate);
router.get('/logout', logout);
router.post('/account/change-password', changePassword);
router.get('/account', getCurrentUser);
router.post('/account/reset-password/init', requestPasswordReset);
router.post('/account/reset-password/finish', finishPasswordReset);

module.exports = router;

function register(req, res, next) {
    accountService.register(req.body)
        .then((user) => res.json(user))
        .catch(err => next(err));
}

function activateUser(req, res, next) {
    accountService.activateUser(req, res)
        .then((user) => res.json(user))
        .catch(err => next(err));
}

function authenticate(req, res, next) {
    console.log(" login ");
    accountService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}


function logout(req, res, next) {
    if(req.user.sub){
        req.user = {};
    }
    res.json({"message":"logot successfuly"})
}

function changePassword(req, res, next) {
    console.log(req.body);
    var passwordVM = req.body;
    if(passwordVM.currentPassword == null || passwordVM.currentPassword == "" || passwordVM.newPassword == null || passwordVM.newPassword == "" && !checkPasswordLength(passwordVM.newPassword)){
        // Throw Invalid Password Exception
        res.json(Exception.InvalidPasswordException())
    }else{
        accountService.changePassword(req, res)
        .then(() => res.json({}))
        .catch(err => next(err));
    }  
}

function checkPasswordLength(password){
    if(password.length > constant.PASSWORD_MIN_LENGTH && password.length <= constant.PASSWORD_MAX_LENGTH){
        return true;
    }else{
        return false;
    }
}

function getCurrentUser(req, res, next) {
    accountService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function requestPasswordReset(req, res, next) {
    if(req.body){
        console.log(req.body)
        accountService.requestPasswordReset(req, res)
        .then((message) => res.json({}))
        .catch(err => next(err));
    }else{
        res.json(Exception.EmailNotFoundException());
    }
}

function finishPasswordReset(req, res, next) {
    var passwordVM = req.body;
    if(passwordVM.key == null || passwordVM.key == "" || passwordVM.newPassword == null || passwordVM.newPassword == "" && !checkPasswordLength(passwordVM.newPassword)){
        // Throw Invalid Password Exception
        res.json(Exception.InvalidPasswordException())
    }else{
        accountService.finishPasswordReset(req, res)
        .then(() => res.json({}))
        .catch(err => next(err));
    }  
}