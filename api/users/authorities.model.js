const mongoose = require('mongoose');
var validate = require('mongoose-validator');
const Schema = mongoose.Schema;

const schema = new Schema({
    authorityName: {
        type: String,
        required: [true, 'Name is required']
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('user_authority', schema);