const config = require("../config/config").defaultConfig;

const jwt = require('jsonwebtoken');
// Create a token generator with the default settings:
var randtoken = require('rand-token');

const userService = require('./user.service');

const sendmail = require('sendmail')();


const bcrypt = require('bcryptjs');
const db = require('../_helpers/db');
const User = db.User;

module.exports = {
    authenticate,
    register,
    activateUser,
    changePassword,
    getById,
    requestPasswordReset,
    finishPasswordReset
};

async function authenticate({ username, password }) {
    console.log(username + "  "+password)
    const user = await User.findOne({ login: username });
    if (user && bcrypt.compareSync(password, user.password)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const id_token = jwt.sign({ sub: user.id }, config.jwt.secret, config.jwt.tokenConfig);
        return {
            ...userWithoutHash,
            id_token
        };
        /*return {
            id_token 
        }*/
    }
}

async function register(userParam) {
    // validate
    if (await User.findOne({ login: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }
    // Generate a 20 character alpha-numeric token:
    userParam.activated = false;
    userParam.activationKey = randtoken.generate(20);
    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.password = bcrypt.hashSync(userParam.password, 10);
    }

    var message = "Hi "+ user.login + "<br/>"
        message = message + "Your activation link is "+  userParam.activationKey;

    sendmail({
            from: 'balasomaske05@gmail.com',
            to: userParam.email,
            subject: 'Activate account',
            html: message,
          }, function(err, reply) {
            console.log(err && err.stack);
            console.dir(reply);
        });
    // save user
    return await user.save();
}

async function activateUser(req, res) {
    if(req.query.key){
        const user = await User.findOne({ activationKey: req.query.key})
        if(user){
            user.activated = true;
            user.activationKey = null;
            await user.save();
        }else{
            return {
                "type" : "https://www.jhipster.tech/problem/problem-with-message",
                "title" : "No user was found for this activation key",
                "status" : 500
              }
        }
    }
}

async function changePassword(req, res) {
    var passwordVM = req.body;
    const user = await userService.getById(req.user.sub);
    if(bcrypt.compareSync(passwordVM.currentPassword, user.password)){
        user.password = bcrypt.hashSync(passwordVM.newPassword, 10);
        user.lastModifiedDate = Date.now();
        return await user.save();
    }else{
     return  res.json( {"error": "Old Password not match"} );
    }
}

async function getById(id) {
    return await userService.getById(id);
}

async function requestPasswordReset(req, res) {
    const user = await User.findOne({ email: req.body})
    if(user){
        // TODO : send mail
        user.resetKey = randtoken.generate(20);
        user.resetDate = Date.now();
        await user.save();
        return {"message":"Reset mail Send"};
    }else{
        return {
            "type" : "problem/email-not-found",
            "title" : "Email address not registered",
            "status" : 400
          }
    }
}

async function finishPasswordReset(req, res) {
    var KeyAndPasswordVM = req.body;
    console.log(KeyAndPasswordVM);
    const user = await User.findOne({resetKey: KeyAndPasswordVM.key} ).select('-hash');
    if(user){
        // TODO : check diff between reset date and current date 
        //user.resetDate && 
        if(true){
        user.password = bcrypt.hashSync(KeyAndPasswordVM.newPassword, 10);
        user.resetKey = "";
        user.resetDate = null;
        await user.save();
        }else{
            user.resetKey = randtoken.generate(20);
            user.resetDate = Date.now();
            await user.save();
            return {"message":"Reset mail Send"};
        }

       console.log(user)
    }else{
        return  {"error": "reset key not match"};
    }   
}



