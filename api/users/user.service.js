/*const config = require('config.json');*/
const config = {
    "connectionString": "mongodb://localhost/nodeproject",
    "secret": "5QdYLbXsONJJDkoEyQPk3aR7vng721veBww9pPglW1tRzp4nnUz3f4AewUPZl9YyyK590vdErx2KBfTu66GOrUOZs6CO9XzMAmQ8hddNqA7oVuV0cUuvEl7Hskqbofgr"
};

const defaultProperties = require("../default/constants");

const bcrypt = require('bcryptjs');
const db = require('../_helpers/db');
const User = db.User;
const paginate = require("../pageable/pageable");
// Create a token generator with the default settings:
var randtoken = require('rand-token');

const UserDTO = "id login firstName lastName email imageUrl activated langKey createdBy createdDate lastModifiedBy lastModifiedDate authorities";

module.exports = {
    getAll,
    getById,
    getByloginName,
    getByEmail,
    create,
    update,
    delete: _delete,
    deleteByLoginName,
    getByResetKey
};

async function getAll(req, res) {
    var paginateParam = paginate.paginationParam(req.query);
    const count = await User.countDocuments();
    res.setHeader("X-Total-Count",count);
    var users = [];
    if(paginateParam.pageNumber == 0){
        users = await User.find().select(UserDTO).limit(paginateParam.pageSize).sort(paginateParam.sortCondition);
    }else{
        users = await User.find().select('UserDTO').skip(paginateParam.pageSize * (paginateParam.pageNumber - 1)).limit(paginateParam.pageSize).sort(paginateParam.sortCondition);
    }
    return users;
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function getByloginName(loginName) {
    return await User.findOne({login: loginName} ).select('UserDTO');
}

async function getByEmail(emailID) {
    return await User.findOne({email: emailID} ).select('UserDTO');
}
async function getByResetKey(resetKey) {
    return await User.findOne({resetKey: resetKey} ).select('UserDTO');
}

async function create(userParam) {
    
    // validate
    if (await User.findOne({ login: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);
    if(user.langKey == null){
        user.langKey = defaultProperties.DEFAULT_LANGUAGE;
    }

    // hash password
    if (userParam.password) {
        user.password = bcrypt.hashSync(userParam.password, 10);
    }

    // Generate a 20 character alpha-numeric token:
    userParam.activated = true;
    userParam.resetKey = randtoken.generate(20);

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.password = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}

async function deleteByLoginName(loginName) {
    await User.remove({ login : loginName});
}