const errorConstanst = require("./ErrorConstants");
const status = require("./Status");

module.exports = EmailAlreadyUsedException;

function EmailAlreadyUsedException(){
    return {
        "type": errorConstanst.EMAIL_ALREADY_USED_TYPE,
        "title": "Email is already in use!",
        "status": "emailexists"
    }
}