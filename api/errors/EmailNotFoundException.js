const errorConstanst = require("./ErrorConstants");
const status = require("./Status");

module.exports = EmailNotFoundException;

function EmailNotFoundException(){
    return {
        "type": errorConstanst.EMAIL_NOT_FOUND_TYPE,
        "title": "Email address not registered",
        "status":  status.BAD_REQUEST
    }
}