
module.exports.ErrorConstants = {
    ERR_CONCURRENCY_FAILURE : "error.concurrencyFailure",
    ERR_VALIDATION: "error.validation",
    PROBLEM_BASE_URL : "baseURL",
    DEFAULT_TYPE :  "/problem-with-message",
    CONSTRAINT_VIOLATION_TYPE :  "/constraint-violation",
    PARAMETERIZED_TYPE :  "/parameterized",
    ENTITY_NOT_FOUND_TYPE :  "/entity-not-found",
    INVALID_PASSWORD_TYPE :  "/invalid-password",
    EMAIL_ALREADY_USED_TYPE :  "/email-already-used",
    LOGIN_ALREADY_USED_TYPE :  "/login-already-used",
    EMAIL_NOT_FOUND_TYPE :  "/email-not-found"
}