const errorConstanst = require("./ErrorConstants").ErrorConstants;
const status = require("./Status").Status;

Exception = {
    InvalidPasswordException,
    EmailNotFoundException
}

module.exports = Exception;

function InvalidPasswordException() {
    return {
        "type": errorConstanst.INVALID_PASSWORD_TYPE,
        "title":  "Incorrect password",
        "status": status.BAD_REQUEST
    }
}

function EmailNotFoundException(){
    return {
        "type": errorConstanst.EMAIL_NOT_FOUND_TYPE,
        "title": "Email address not registered",
        "status":  status.BAD_REQUEST
    }
}