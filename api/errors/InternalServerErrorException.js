const errorConstanst = require("./ErrorConstants");
const status = require("./Status");

module.exports = InternalServerErrorException;

function InternalServerErrorException(message){
    return {
        "type": errorConstanst.DEFAULT_TYPE,
        "title":  message,
        "status": status.INTERNAL_SERVER_ERROR
    }
}