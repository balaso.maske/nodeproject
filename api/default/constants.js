module.exports.Constant = {
    LOGIN_REGEX : "^[_.@A-Za-z0-9-]*$",
    SYSTEM_ACCOUNT: "system",
    ANONYMOUS_USER: "anonymoususer",
    DEFAULT_LANGUAGE: "en",
    PASSWORD_MIN_LENGTH: 4,
    PASSWORD_MAX_LENGTH: 100
}
 