// middleware for authentication

const userService = require("../users/user.service");

module.exports = setCurrentUser;

function authorize (req, res, next) {
    const apiToken = req.headers['Authorization'];
    console.log(req.db);
    
    // set user on-success
    req.user = req.db.users.findByApiKey(apiToken);
    console.log(request.user);
       
    // always continue to next middleware
    next();
  }

async function setCurrentUser(req, res, next) {
    const user = await userService.getById(userID);
    return 
    req.currentUser = user;
    console.log(user);
     // always continue to next middleware
     next();
};