const express = require('express');
const router = express.Router();
const userService = require('../users/user.service');


// routes
router.post('/authenticates', authenticate);

module.exports = router;

function authenticate(req, res, next) {
    console.log("auth");
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}