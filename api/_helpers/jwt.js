const expressJwt = require('express-jwt');

const config = require("../config/config").defaultConfig;

const userService = require('../users/user.service');

module.exports = jwt;

function jwt() {
    const secret = config.jwt.secret;
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes that don't require authentication
            '/api/authenticate',
            '/api/register',
            '/api/activate',
            '/api/account/reset-password/init',
            '/api/account/reset-password/finish'
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);
    req.user = user;

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }
    done();
};