
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('./api/_helpers/jwt');
const errorHandler = require('./api/_helpers/error-handler');
const setCurrentUser = require("./api/auth/authentication");


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use('/api', require('./api/users/users.controller'));
app.use('/api', require('./api/users/account.controller'));
app.use('/api', require('./api/auth/authController'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? 3006 : 4444;

const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
